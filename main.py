import subprocess
import json
from vds_ansible.vds_api.vds_api import VDS_API


def get_config(debug):
    if debug:
        f = open('vds_config.json')
        data = json.load(f)
        config = {
            "token": data["token"],
            "secure": data["secure"]
        }
    return config


def get_servers_config(debug):
    if debug:
        f = open('vds_servers.json')
        data = json.load(f)
        config = []
        for server in data["servers"]:
            config.append(server)
    return config


def make_hosts(ip):
    print(ip)
    f = open("vds_ansible/hosts", "w")
    f.write("[masters]\n")
    f.write("master ansible_host={master_ip} ansible_user=root\n\n".format(master_ip = ip[0]))

    f.write("[workers]\n")
    for i in range(1,3):
        f.write("{workerid} ansible_host={worker_ip} ansible_user=root\n".format(workerid = "worker"+str(i), worker_ip = ip[i]))
    f.write("\n[all:vars]\n")
    f.write("ansible_python_interpreter=/usr/bin/python3\n")
    f.close()


def start_k8s(path):
    subprocess.run([path, "arguments"], shell=True)


def control_panel(vds_api):
    config_servers = get_servers_config(True)
    while True:
        print("\ncreate - for create servers for clusters")
        print("hosts - create hosts file for k8s cluster")
        print("k8s - automatic creation and configuration k8s cluster")
        print("K88S - уxecutes all commands listed above")
        print("delete - delete servers")
        print("exit - for exit(ctrl + c)")
        key = input("input command - ")
        if key == "create":
            print("Starting creare servers...")
            vds_api.create_servers_js(config_servers)
            print("Done")
        elif key == "hosts":
            print("Starting make hosts file...")
            make_hosts(vds_api.get_ip())
            print("Done")
        elif key == "k8s":
            print("Starting create k8s cluster...")
            start_k8s("./start.sh")
            print("Done")
        elif key == "K88S":
            print("Starting Creare servers...")
            vds_api.create_servers_js(config_servers)
            print("Done")
            print("Starting make hosts file...")
            make_hosts(vds_api.get_ip())
            print("Done")
            print("Starting create k8s cluster...")
            start_k8s("./start.sh")
            print("Done")
        elif key == "delete":
            print("Starting Delete servers...")
            vds_api.secure_delete_servers()
            print("Done")
        elif key == "exit":
            break
        elif key == "sas":
            pass
        else:
            print("\nWrong input\n")


if __name__ == '__main__':
    config = get_config(True)
    control_panel(VDS_API(config))