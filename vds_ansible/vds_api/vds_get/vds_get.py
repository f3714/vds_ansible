import requests
import json


class VDS_GET:
    def __init__(self, token, url, headers):
        self.token = token
        self.url = url
        self.headers = headers

    def get_account(self):
        url = self.url + 'account'
        return requests.get(url, headers=self.headers).json()

    def get_servers_all(self):
        url = self.url + 'scalets'
        return requests.get(url, headers=self.headers).json()

    def get_servers_filter(self, param = 'ctid'):
        data = self.get_servers_all()
        result = []
        for record in data:
            result.append(record[param])
        return result