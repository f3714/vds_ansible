import requests
import json
import time
from vds_ansible.vds_api.vds_get.vds_get import VDS_GET


class VDS_API:
    def __init__(self, config):
        self.token = config["token"]
        self.secure_server = config["secure"]
        self.url = "https://api.vscale.io/v1/"
        self.headers = {
                'X-Token': config["token"]
        }
        self.VDS_GET = VDS_GET(self.token, self.url, self.headers)

    
    def create_server(self, server):
        url = self.url + 'scalets/'
        headers = self.headers
        headers['Content-Type'] = 'application/json;charset=UTF-8'
        data = {
            "make_from":server["make_from"],
            "rplan":server["rplan"],
            "do_start":server["do_start"],
            "name":server["name"],
            "keys":server["keys"],
            "location":server["location"]
        }
        data_json = json.dumps(data)
        data_json = data_json.replace(' ', '')
        return requests.post(url, headers=headers, data=data_json).json()
    

    def delete_server(self, ctid):
        url = self.url + 'scalets/' + str(ctid)
        headers = self.headers
        headers['Content-Type'] = 'application/json;charset=UTF-8'
        return requests.delete(url, headers=headers,).json()


    def create_servers_js(self, servers):
        for server in servers:
            print(self.create_server(server))
            time.sleep(3)
        return 0
    

    def secure_delete_servers(self):
        servers = self.VDS_GET.get_servers_filter()
        for server in servers:
            if server == self.secure_server["ctid"]:
                pass
            else:
                print(self.delete_server(server))
        return 0

    def get_ip(self):
        servers = self.VDS_GET.get_servers_filter("public_address")
        ip = []
        for ips in servers:
            if ips['address'] == self.secure_server["ip"]:
                pass
            else:
                ip.append(ips['address'])
        return ip
