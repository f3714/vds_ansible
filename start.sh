ansible-playbook -i ./vds_ansible/hosts ./vds_ansible/kube-cluster/initial.yaml
ansible-playbook -i ./vds_ansible/hosts ./vds_ansible/kube-cluster/kube-dependencies.yaml
ansible-playbook -i ./vds_ansible/hosts ./vds_ansible/kube-cluster/master.yaml
ansible-playbook -i ./vds_ansible/hosts ./vds_ansible/kube-cluster/workers.yaml