# Vds_ANSIBLE


## Getting started
It is my tool for automatic creation and configuration k8s on vds hosting platform. For create this project I use my previous repository VDS_REST_SCRIPTS, but he private.

If you want to use this code for yourself you need to take into account a few conditions:
- Change example_vds_config.json and example_vds_servers.json files for your configuration
- Then edit get_config() get_servers_config() functions in main.py
- And install requirements for python(requirements.txt)

For launch use:
```
python main.py
```